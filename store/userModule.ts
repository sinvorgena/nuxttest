
import {store} from "./index";
import {IDictionary, Item, ItemDictionary} from "~/types/types";
import {VuexModule, Module, Mutation, Action} from 'vuex-class-modules';

@Module
export class UserModule extends VuexModule {
  items = {} as ItemDictionary
  basket = {} as IDictionary<number>

  @Mutation
  setItems(items: ItemDictionary) {
    this.items = {...items}
  }
  @Mutation
  setBasket(basket: IDictionary<number>) {
    this.basket = {...basket}
  }

  @Mutation
  setIncrementedItemToBasket(id: number) {
    if (typeof this.basket[id] === 'undefined') {
      this.basket[id] = 1
    } else {
      this.basket[id]++
    }
    this.basket = {...this.basket}

    localStorage.setItem("basket", JSON.stringify(this.basket))

  }

  @Mutation
  setDecrementedItemFromBasket(id: number) {
    if (this.basket[id] === 1) {
      delete this.basket[id]
    } else {
      this.basket[id]--
    }
    this.basket = {...this.basket}

    localStorage.setItem("basket", JSON.stringify(this.basket))
  }


  @Action
  async loadItems() {
    const response = await fetch("https://fakestoreapi.com/products")
    const items = await response.json()
    const filteredItems = items.filter((item: Item) => {
      if (typeof item.title === 'undefined' || typeof item.price === 'undefined') {
        return false
      }
      return true
    })
    const formatedItems = Object.fromEntries(filteredItems.map((item: Item) => {
      const id = Number(item.id)
      delete item.id
      return [id, item];
    }))

    this.setItems(formatedItems)
    return formatedItems
  }

  @Action
  async loadBasket(storage: string) {
    let basket = JSON.parse(storage)
    this.setBasket(basket)
    return basket
  }
}


export const userModule = new UserModule({store, name: "user"});
