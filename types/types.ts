export interface IDictionary<TValue> {
  [id: number]: TValue;
}

export interface ItemDictionary {
  [id: number]: Item;
}

export interface Rating {
  rete: number,
  count: number
}

export interface Item {
  title: string,
  price: number,
  description: string,
  category: string,
  image: string,
  rating: Rating,
  id?: number
}
